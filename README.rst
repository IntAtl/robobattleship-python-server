RoboBattleship game server
==========================

A simple Battleship game server which provides a REST API for players to
register, connect and fight with other players.

- Website - http://robobattleship.com
- GitHub - https://github.com/apopelo/robobattleship


Building docker
Exec command in the root of robobattelship directory
docker build -t robobattleship-server:latest .

Running docker
docker run --dns 172.17.0.1 -d -p 9999:9999 --name robobattleship-server robobattleship-server:latest


Starting the server
-------------------

You need to have libevent and python-dev installed in your system.

Create virtualenv::

    $ mkvirtualenv robobattleship

Install requirements into virtualenv::

    $ workon robobattleship && pip install -r requirements.pip

Run the server::

    $ python robobattleship


Authors
-------

Original implementation by:

- Andrey Popelo <andrey@popelo.com>
- Sergey Lysach <sergikoff88@gmail.com>

Big thanks for idea and help to Sergey Lysach
