FROM python:2.7.7
ADD ./ /opt/
RUN pip install -r /opt/requirements.pip
CMD [ "python", "/opt/robobattleship" ]
